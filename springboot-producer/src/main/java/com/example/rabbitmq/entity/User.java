package com.example.rabbitmq.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {

    private int id;
    private String userName;
    private String passWord;
    private String realName;

    public User(int id,String userName,String passWord,String realName){
        this.id = id;
        this.userName = userName;
        this.passWord = passWord;
        this.realName = realName;
    }
}
