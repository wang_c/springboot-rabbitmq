package com.example.rabbitmq.controller;

import com.example.rabbitmq.MessageProducer;
import com.example.rabbitmq.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rabbit")
public class testController {

    @Autowired
    private MessageProducer messageProducer;

    @PostMapping("/send")
    public void hello() {
        messageProducer.sendMessage(new User(1,"aa","aa","aa"));
    }
}
